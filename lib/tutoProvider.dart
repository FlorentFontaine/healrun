import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Future<Tuto> fetchTuto() async {
  final response = await http
      .get(Uri.parse('https://jsonplaceholder.typicode.com/albums/1'));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return Tuto.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load Tuto');
  }
}

class Tuto {
  final int userId;
  final int id;
  final String title;

  const Tuto({
    required this.userId,
    required this.id,
    required this.title,
  });

  factory Tuto.fromJson(Map<String, dynamic> json) {
    return Tuto(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
    );
  }
}
