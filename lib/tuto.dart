import 'package:flutter/material.dart';

import 'tuto.dart';
import 'tutoProvider.dart';

class Tuto extends StatefulWidget {
  const Tuto({Key? key}) : super(key: key);

  @override
  State<Tuto> createState() => _TutoState();
}

class _TutoState extends State<Tuto> {
  late Future<Tuto> futureTuto;

  @override
  void initState() {
    super.initState();
    futureTuto = fetchTuto() as Future<Tuto>;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fetch Data Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Fetch Data Example'),
        ),
        body: Center(
          child: FutureBuilder<Tuto>(
            future: futureTuto,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Text(snapshot.data!.toString());
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }

              // By default, show a loading spinner.
              return const CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}
