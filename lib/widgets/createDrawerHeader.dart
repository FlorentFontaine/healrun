import 'package:flutter/material.dart';

Widget createDrawerHeader() {
  return DrawerHeader(
      margin: EdgeInsets.zero,
      padding: const EdgeInsets.symmetric(vertical: 18.0),
      child: Stack(children: const <Widget>[
        Positioned(
            bottom: 12.0,
            left: 16.0,
            child: Text("HealRun",
                style: TextStyle(
                    color: Colors.green,
                    fontSize: 25.0,
                    fontWeight: FontWeight.w600))),
      ]));
}

Widget createDrawerBodyItem(
    {IconData? icon,
    required String text,
    colorText,
    GestureTapCallback? onTap}) {
  return ListTile(
    title: Row(
      children: <Widget>[
        Icon(icon),
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(text, style: const TextStyle(color: Colors.white)),
        )
      ],
    ),
    onTap: onTap,
  );
}
