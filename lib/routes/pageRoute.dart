import 'package:HealRun/pages/profile/profilePage.dart';
import 'package:HealRun/pages/profile/contactPage.dart';
import 'package:HealRun/pages/profile/notificationPage.dart';
import 'package:HealRun/pages/profile/eventPage.dart';

import '../myHomePage.dart';

// ignore: camel_case_types
class pageRoutes {
  static const String home = MyHomePage.routeName;
  static const String contact = contactPage.routeName;
  static const String event = eventPage.routeName;
  static const String profile = profilePage.routeName;
  static const String notification = notificationPage.routeName;
}
