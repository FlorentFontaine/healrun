import 'package:flutter/material.dart';
import 'dart:developer' as dev;

import '../navigationDrawer/navigationDrawer.dart';

class Coaching extends StatefulWidget {
  const Coaching({Key? key}) : super(key: key);

  @override
  State<Coaching> createState() => _Coaching();
}

class _Coaching extends State<Coaching> {
  @override
  void initState() {
    super.initState();
    dev.log('initState()', name: 'coaching');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Coaching'),
        backgroundColor: const Color.fromRGBO(1, 27, 85, 1),
        centerTitle: true, // this is al
      ),
      drawer: const navigationDrawer(),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return ListTile(
            title: Text('Tile #$index'),
          );
        },
        itemCount: 20,
      ),
    );
  }
}
