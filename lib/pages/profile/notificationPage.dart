import 'package:flutter/material.dart';

import '../../navigationDrawer/navigationDrawer.dart';

// ignore: camel_case_types
class notificationPage extends StatelessWidget {
  static const String routeName = '/notificationPage';

  const notificationPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Notifications"),
        ),
        drawer: const navigationDrawer(),
        body: const Center(child: Text("Les notifications en attente")));
  }
}
