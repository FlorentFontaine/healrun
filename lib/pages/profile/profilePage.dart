import 'package:flutter/material.dart';

import '../../navigationDrawer/navigationDrawer.dart';

class profilePage extends StatelessWidget {
  static const String routeName = '/profilePage';

  const profilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Profil"),
        ),
        drawer: const navigationDrawer(),
        body: const Center(child: Text("Profil à travailler")));
  }
}
