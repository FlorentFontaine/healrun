import 'package:flutter/material.dart';

import '../../navigationDrawer/navigationDrawer.dart';

// ignore: camel_case_types
class eventPage extends StatelessWidget {
  static const String routeName = '/eventPage';

  const eventPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Evénements"),
        ),
        drawer: const navigationDrawer(),
        body: const Center(child: Text("Les événements à venir")));
  }
}
