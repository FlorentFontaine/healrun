import 'package:flutter/material.dart';
import 'dart:developer' as dev;

import '../navigationDrawer/navigationDrawer.dart';

class Sante extends StatefulWidget {
  const Sante({Key? key}) : super(key: key);

  @override
  State<Sante> createState() => _Sante();
}

class _Sante extends State<Sante> {
  @override
  void initState() {
    super.initState();
    dev.log('initState()', name: 'sante');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sante'),
        backgroundColor: const Color.fromRGBO(255, 0, 114, 1),
        centerTitle: true, // this is al
      ),
      drawer: const navigationDrawer(),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return ListTile(
            title: Text('Tile #$index'),
          );
        },
        itemCount: 20,
      ),
    );
  }
}
