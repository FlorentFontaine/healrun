import 'package:flutter/material.dart';
import 'dart:developer' as dev;

import '../navigationDrawer/navigationDrawer.dart';

class Entrainement extends StatefulWidget {
  const Entrainement({Key? key}) : super(key: key);

  @override
  State<Entrainement> createState() => _Entrainement();
}

class _Entrainement extends State<Entrainement> {
  @override
  void initState() {
    super.initState();
    dev.log('initState()', name: 'entrainement');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Entrainement'),
        backgroundColor: Colors.greenAccent,
        centerTitle: true, // this is al
      ),
      drawer: const navigationDrawer(),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return ListTile(
            title: Text('Tile #$index'),
          );
        },
        itemCount: 20,
      ),
    );
  }
}
