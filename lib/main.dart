import 'package:HealRun/pages/profile/contactPage.dart';
import 'package:HealRun/pages/profile/eventPage.dart';
import 'package:HealRun/pages/profile/notificationPage.dart';
import 'package:HealRun/pages/profile/profilePage.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import '/components/lazy_indexed_stack.dart';
import '/pages/entrainement.dart';
import '/pages/sante.dart';
import '/pages/coaching.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:go_router/go_router.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'myHomePage.dart';
import 'routes/pageRoute.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyApp();
}

class _MyApp extends State<MyApp> {
  @override
  Widget build(BuildContext context) => MaterialApp.router(
        routeInformationParser: _router.routeInformationParser,
        routerDelegate: _router.routerDelegate,
        title: 'HealRun',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
      );

  late final GoRouter _router = GoRouter(
    routes: [
      GoRoute(
        path: '/',
        redirect: (state) => '/accueil',
      ),
      GoRoute(
        path: '/accueil',
        pageBuilder: (context, state) => _build(0),
      ),
      GoRoute(
        path: '/coaching',
        pageBuilder: (context, state) => _build(1),
      ),
      GoRoute(
        path: '/sante',
        pageBuilder: (context, state) => _build(2),
      ),
      GoRoute(
        path: '/entrainement',
        pageBuilder: (context, state) => _build(3),
      ),
      GoRoute(
        path: '/homePage',
        pageBuilder: (context, state) => _build(0),
      ),
      GoRoute(
        path: '/profilePage',
        builder: (context, state) => const profilePage(),
      ),
      GoRoute(
        path: '/contactPage',
        builder: (context, state) => const contactPage(),
      ),
      GoRoute(
        path: '/eventPage',
        builder: (context, state) => const eventPage(),
      ),
      GoRoute(
        path: '/notificationPage',
        builder: (context, state) => const notificationPage(),
      ),
    ],
    debugLogDiagnostics: true,
  );

  Page _build(int currentIndex) {
    return NoTransitionPage<void>(
      child: Scaffold(
        body: LazyIndexedStack(
          index: currentIndex,
          children: const [
            MyHomePage(),
            Coaching(),
            Sante(),
            Entrainement(),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          selectedIconTheme: const IconThemeData(color: Colors.greenAccent),
          currentIndex: currentIndex,
          onTap: (value) {
            switch (value) {
              case 0:
                return _router.go('/accueil');
              case 1:
                return _router.go('/coaching');
              case 2:
                return _router.go('/sante');
              case 3:
                return _router.go('/entrainement');
            }
          },
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Colors.greenAccent,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "Accueil",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.sports_gymnastics),
              label: "Coaching",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.heart_broken),
              label: "Sante",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.directions_run),
              label: "Entrainement",
            ),
          ],
        ),
      ),
    );
  }
}
