import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'navigationDrawer/navigationDrawer.dart';
import 'tutoProvider.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  static const String routeName = '/homePage';

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        child: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            title: const Text('HealRun'),
          ),
          drawer: const navigationDrawer(),
          body: TabBarView(
            children: [
              Center(
                  child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                    height: 650,
                    width: 300,
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(40.0),
                        border:
                            Border.all(color: Colors.greenAccent, width: 2.0)),
                    child: Center(
                      child: Text(
                        textAlign: TextAlign.center,
                        'Accueil',
                        style: TextStyle(
                          fontSize: 40,
                          color: Colors.grey[300],
                        ),
                      ),
                    )),
              )),
              Center(
                  child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                    height: 650,
                    width: 300,
                    decoration: BoxDecoration(
                        color: const Color.fromRGBO(255, 0, 114, 1),
                        borderRadius: BorderRadius.circular(40.0),
                        border:
                            Border.all(color: Colors.greenAccent, width: 2.0)),
                    child: Center(
                      child: Text(
                        textAlign: TextAlign.center,
                        'Sante',
                        style: TextStyle(
                          fontSize: 40,
                          color: Colors.grey[300],
                        ),
                      ),
                    )),
              )),
              Center(
                  child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                    height: 650,
                    width: 300,
                    decoration: BoxDecoration(
                        color: Colors.greenAccent,
                        borderRadius: BorderRadius.circular(40.0),
                        border:
                            Border.all(color: Colors.greenAccent, width: 2.0)),
                    child: Center(
                      child: Text(
                        textAlign: TextAlign.center,
                        'Entrainement',
                        style: TextStyle(
                          fontSize: 40,
                          color: Colors.grey[300],
                        ),
                      ),
                    )),
              )),
              Center(
                  child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                    height: 650,
                    width: 300,
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(1, 27, 85, 1),
                        borderRadius: BorderRadius.circular(40.0),
                        border:
                            Border.all(color: Colors.greenAccent, width: 2.0)),
                    child: Center(
                      child: Text(
                        textAlign: TextAlign.center,
                        'Coaching',
                        style: TextStyle(
                          fontSize: 40,
                          color: Colors.grey[300],
                        ),
                      ),
                    )),
              )),
            ],
          ),
        ));
  }
}
