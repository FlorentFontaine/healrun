import 'package:flutter/material.dart';
import 'package:HealRun/widgets/createDrawerHeader.dart';
import 'package:HealRun/routes/pageRoute.dart';
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:go_router/go_router.dart';

// ignore: camel_case_types
class navigationDrawer extends StatelessWidget {
  const navigationDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Container(
      color: Colors.black,
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          createDrawerHeader(),
          createDrawerBodyItem(
              icon: Icons.home,
              text: 'Accueil',
              onTap: () => context.go(pageRoutes.home)),
          createDrawerBodyItem(
              icon: Icons.account_circle,
              text: 'Profil',
              onTap: () => context.go(pageRoutes.profile)),
          createDrawerBodyItem(
              icon: Icons.event_note,
              text: 'Evenement',
              onTap: () => context.go(pageRoutes.event)),
          const Divider(color: Colors.white),
          createDrawerBodyItem(
              icon: Icons.notifications_active,
              text: 'Notifications',
              onTap: () => context.go(pageRoutes.notification)),
          createDrawerBodyItem(
              icon: Icons.contact_phone,
              text: 'Contact',
              onTap: () => context.go(pageRoutes.contact)),
        ],
      ),
    ));
  }
}
